# Sandbox Quick Deploy

Need a quick and reliable method for standing up a test instance? Quickly deploy
to AWS (GCP and potentially other providers planned) in just a few minutes. Just
add your preferred initial config values and run `tofu apply` to go.

## Prerequisites

### OpenTofu/Terraform

Install OpenTofu by following the instructions here: 
[Installing OpenTofu](https://opentofu.org/docs/intro/install/), or 
[Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
if that's your speed. This documentation and others in this project will assume
OpenTofu, but the two are interchangeable.

### AWS CLI

Either install from your distro's package repo, or via the instructions posted
here: [Install or update to the latest version of the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html),
and authenticated to AWS. OpenTofu will use the credentials stored locally for
provisioning resources.

### DNS

You must have a domain set up in Route 53. 

### SSH Key

Generate and upload a named SSH key to AWS.

### License File

Follow SOP for generating a team member license.

## Usage

1. Update your values either directly in `variables.tf`, or preferably in
`user.auto.tfvars`, which will override the values in `variables.tf`.
1. Drop into `/aws` and run `tofu init` to initialize the module. 
1. Run `tofu plan` to see what actions will be taken.
1. Run `tofu apply` and type `yes` when prompted to begin provisioning.

If everything goes well, OpenTofu should finish within a minute or two.
It will still take the server a few more minutes to run the user data script and
for GitLab to come up. Astute readers will note a manual sleep in the user data
for the runner instance to give the server instance time to install GitLab and
settle down. Assuming no problems, expect total time from `tofu apply` to a
fully provisioned set up to be about 20 minutes.