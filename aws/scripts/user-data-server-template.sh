#!/bin/bash
apt update && apt install -y curl openssh-server ca-certificates tzdata perl dnsutils

# Grab Public IP from AWS EC2 metadata
aws_token=$(curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600")
server_ip=$(curl -H "X-aws-ec2-metadata-token: $aws_token" http://169.254.169.254/latest/meta-data/public-ipv4)

# Confirm DNS configured
dns_check=''
dns_check=$(dig +short "${hostname}")
until [[ $dns_check == "$server_ip" ]]; do
    sleep 5s
    dns_check=$(dig +short "${hostname}")
done
dns_check=$(dig +short "${pages_domain}")
until [[ $dns_check == "$server_ip" ]]; do
    sleep 5s
    dns_check=$(dig +short "${pages_domain}")
done

# Generate Pages cert
snap install --classic certbot
ln -s /snap/bin/certbot /usr/bin/certbot
certbot certonly -d "${pages_domain}" --standalone --register-unsafely-without-email --non-interactive --agree-tos

# Install GitLab package repo
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | bash

# Install GitLab
EXTERNAL_URL="https://${hostname}" GITLAB_ROOT_PASSWORD="${initial_root_password}" apt install -y gitlab-ee=${install_version}-ee.0

gitlab-ctl reconfigure

# Copy over Pages cert to /etc/gitlab/ssl/
cp "/etc/letsencrypt/live/${pages_domain}/cert.pem" "/etc/gitlab/ssl/pages-nginx.crt"
cp "/etc/letsencrypt/live/${pages_domain}/privkey.pem" "/etc/gitlab/ssl/pages-nginx.key"

# Switch to clean gitlab.rb and add basic config
mv "/etc/gitlab/gitlab.rb" "/etc/gitlab/gitlab.rb.old"
touch "/etc/gitlab/gitlab.rb"

cat << EOF >> "/etc/gitlab/gitlab.rb"
gitlab_rails['env'] = { "GITLAB_LICENSE_MODE" => "test", "CUSTOMER_PORTAL_URL" => "https://customers.staging.gitlab.com" }
external_url 'https://${hostname}'
pages_external_url 'https://${pages_domain}'
gitlab_rails['monitoring_whitelist'] = ['${runner_ip}']
pages_nginx['enable'] = true
gitlab_pages['namespace_in_path'] = true
EOF

gitlab-ctl reconfigure

# Settings below added after Pages has been configured first
cat << EOF >> "/etc/gitlab/gitlab.rb"
gitlab_pages['access_control'] = true
gitlab_pages['auth_redirect_uri'] = 'https://${pages_domain}/projects/auth'
pages_nginx['redirect_http_to_https'] = true
pages_nginx['ssl_certificate'] = '/etc/gitlab/ssl/pages-nginx.crt'
pages_nginx['ssl_certificate_key'] = '/etc/gitlab/ssl/pages-nginx.key'
EOF

gitlab-ctl reconfigure

# Insert access token for runner setup
gitlab-rails runner "token = User.find_by_username('root').personal_access_tokens.create(scopes: ['create_runner'], name: 'Runner automation token', expires_at: 365.days.from_now); token.set_token('${pat_token}'); token.save!"

# Apply license
gitlab-rails runner "/tmp/apply_license.rb"