#!/bin/bash
apt update && apt install -y curl jq

# Install GitLab Runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
apt update
apt-cache madison gitlab-runner
apt install -y gitlab-runner=${install_version}

# Sleep to allow server setup to settle
sleep 19m

# Confirm GitLab OK before proceeding
health_check=''
health_check=$(curl --insecure "https://${server_domain}/-/health")
until [[ $health_check == "GitLab OK" ]]; do
    sleep 1m
    health_check=$(curl --insecure "https://${server_domain}/-/health")
done

# Create runner config via API
response=$(curl --insecure --silent --request POST --url "https://${server_domain}/api/v4/user/runners" --data "runner_type=instance_type" --data "tag_list=shared" --header "PRIVATE-TOKEN: ${pat_token}")
runner_registration_token=$(jq '.token' <<< "$response")
runner_registration_token="$${runner_registration_token:1:25}"

# Register basic shell (bash) runner with registration token
# Using a runner authentication token with --registration-token will be
# deprecated in Runner 18. More info:
# https://docs.gitlab.com/ee/ci/runners/new_creation_workflow.html#changes-to-the-gitlab-runner-register-command-syntax
gitlab-runner register --non-interactive --url "https://${server_domain}" --registration-token "$${runner_registration_token}" --executor "shell" --shell "bash" --name "default_shared_runner" --run-untagged