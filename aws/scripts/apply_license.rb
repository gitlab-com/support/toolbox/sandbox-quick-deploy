license_file = File.open("/tmp/Gitlab.gitlab-license")
key = license_file.read.gsub("\r\n", "\n").gsub(/\n+$/, '') + "\n"
license = License.new(data: key)
license.save