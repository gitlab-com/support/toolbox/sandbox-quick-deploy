output "gitlab_public_domain" {
  description = "GitLab public domain for UI access"
  value       = local.prefixed_server_fdqn
}

output "initial_root_password" {
  description = "Password for root account"
  value       = var.gitlab_settings.initial_root_password
}

output "pat_token" {
  description = "Personal access token for runner creation and registration"
  value       = random_string.pat_token.result
}

output "server_ssh_string" {
  description = "SSH string for connecting to server instance"
  value       = "ssh ubuntu@${aws_instance.instance_gitlab_server.public_ip} -i ${var.ssh_key_local_path}"
}

output "runner_ssh_string" {
  description = "SSH string for connecting to runner instance"
  value       = "ssh ubuntu@${aws_instance.instance_gitlab_runner.public_ip} -i ${var.ssh_key_local_path}"
}