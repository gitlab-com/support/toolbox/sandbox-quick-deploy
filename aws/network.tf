resource "aws_route53_record" "gitlab_server" {
  zone_id         = var.aws_hosted_zone["zone_id"]
  name            = local.prefixed_server_fdqn
  type            = "A"
  ttl             = 300
  records         = [aws_instance.instance_gitlab_server.public_ip]
  allow_overwrite = true
}

resource "aws_route53_record" "gitlab_pages" {
  zone_id = var.aws_hosted_zone["zone_id"]
  # name            = "*.${var.domain_names["gitlab_pages_domain"]}.${var.aws_hosted_zone["name"]}"
  name            = local.prefixed_pages_fqdn
  type            = "A"
  ttl             = 300
  records         = [aws_instance.instance_gitlab_server.public_ip]
  allow_overwrite = true
}
