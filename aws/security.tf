resource "aws_default_vpc" "default" {
}

resource "aws_security_group" "sg_gitlab" {
  name        = "${random_string.prefix.result}-sg_gitlab"
  description = "Allow inbound/outbound traffic on 80, 443, 22"
  vpc_id      = aws_default_vpc.default.id

  tags = {
    name = "${random_string.prefix.result}-sg_gitlab"
  }
}

resource "aws_vpc_security_group_egress_rule" "allow_all_traffic_ipv4" {
  security_group_id = aws_security_group.sg_gitlab.id
  cidr_ipv4         = "0.0.0.0/0"
  ip_protocol       = "-1" # semantically equivalent to all ports
}

resource "aws_vpc_security_group_ingress_rule" "allow_https_ipv4" {
  security_group_id = aws_security_group.sg_gitlab.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 443
  ip_protocol       = "tcp"
  to_port           = 443
}

resource "aws_vpc_security_group_ingress_rule" "allow_http_ipv4" {
  security_group_id = aws_security_group.sg_gitlab.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 80
  ip_protocol       = "tcp"
  to_port           = 80
}

resource "aws_vpc_security_group_ingress_rule" "allow_ssh_ipv4" {
  security_group_id = aws_security_group.sg_gitlab.id
  cidr_ipv4         = var.user_ipv4_address
  from_port         = 22
  ip_protocol       = "tcp"
  to_port           = 22
}
