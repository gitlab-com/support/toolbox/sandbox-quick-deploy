variable "aws_region" {
  description = "AWS region"
  type        = map(string)
  default = {
    region            = "us-west-1"
    availability_zone = "us-west-1a"
  }
}

variable "aws_hosted_zone" {
  description = "Holds relevant hosted zone info"
  type        = map(string)
}

variable "domain_names" {
  description = "Holds preferred domain names for resources"
  type        = map(string)
  default = {
    gitlab_server_subdomain = "gitlab"
    gitlab_pages_subdomain  = "gitlab-pages"
  }
}

variable "ssh_key_name" {
  description = "Holds SSH key name"
  type        = string
}

variable "ssh_key_local_path" {
  description = "Local path to SSH key"
  type        = string
}

variable "user_ipv4_address" {
  description = "User IP address for SSH ingress"
  type        = string
}

variable "gitlab_settings" {
  description = "GitLab-specific settings"
  type        = map(string)
  default = {
    local_license_path = "./GitLab.gitlab-license"
  }
}

variable "ec2_sizes" {
  description = "Instance size settings"
  type        = map(string)
  default = {
    server_size = "c5.xlarge"
    runner_size = "t2.medium"
  }
}