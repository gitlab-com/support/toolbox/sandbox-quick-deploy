data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

locals {
  prefixed_server_fdqn = "${random_string.prefix.result}-${var.domain_names["gitlab_server_subdomain"]}.${var.aws_hosted_zone["name"]}"
  prefixed_pages_fqdn  = "${random_string.prefix.result}-${var.domain_names["gitlab_pages_subdomain"]}.${var.aws_hosted_zone["name"]}"
  server_user_data = templatefile("./scripts/user-data-server-template.sh", {
    install_version       = var.gitlab_settings["server_version"]
    initial_root_password = var.gitlab_settings["initial_root_password"]
    hostname              = local.prefixed_server_fdqn
    pages_domain          = local.prefixed_pages_fqdn
    pat_token             = random_string.pat_token.result
    runner_ip             = aws_instance.instance_gitlab_runner.public_ip
    license_path          = "/tmp/Gitlab.gitlab-license"
  })
  runner_user_data = templatefile("./scripts/user-data-runner-template.sh", {
    install_version = var.gitlab_settings["runner_version"]
    pat_token       = random_string.pat_token.result
    server_domain   = local.prefixed_server_fdqn
  })
}

resource "random_string" "prefix" {
  length  = 5
  special = false
  numeric = false
  upper   = false
}

resource "random_string" "pat_token" {
  length = 20
  special = false
  numeric = false
}

resource "aws_instance" "instance_gitlab_server" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.ec2_sizes["server_size"]
  user_data              = local.server_user_data
  key_name               = var.ssh_key_name
  vpc_security_group_ids = [aws_security_group.sg_gitlab.id]

  provisioner "file" {
    source      = var.gitlab_settings["local_license_path"]
    destination = "/tmp/Gitlab.gitlab-license"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file(var.ssh_key_local_path)
    }
  }

  provisioner "file" {
    source      = "./scripts/apply_license.rb"
    destination = "/tmp/apply_license.rb"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file(var.ssh_key_local_path)
    }
  }

  root_block_device {
    volume_size = 32
  }

  tags = {
    name = "${random_string.prefix.result}-in_gitlab_server"
  }
}

resource "aws_instance" "instance_gitlab_runner" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.ec2_sizes["runner_size"]
  user_data              = local.runner_user_data
  key_name               = var.ssh_key_name
  vpc_security_group_ids = [aws_security_group.sg_gitlab.id]

  root_block_device {
    volume_size = 16
  }

  tags = {
    name = "${random_string.prefix.result}-in_gitlab_runner"
  }
}
